use alloc::vec::Vec;

use cryptographic_hash_function::CryptographicHashFunction;
use keccak::KeccakF1600;

pub struct Sha3_256(KeccakF1600);

impl Sha3_256 {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Default for Sha3_256 {
    fn default() -> Self {
        Self(KeccakF1600::new(1088, 512, 256 / 8))
    }
}

impl CryptographicHashFunction for Sha3_256 {
    fn hash_to_bytes(&mut self, message: &[u8]) -> Vec<u8> {
        self.0.keccak(message, 0x06)
    }
}

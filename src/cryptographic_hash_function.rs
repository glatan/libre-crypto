#[cfg(feature = "blake")]
pub use blake::{Blake28, Blake32, Blake48, Blake64, Blake224, Blake256, Blake384, Blake512};
#[cfg(feature = "blake2")]
pub use blake2::{Blake2b, Blake2s};
#[cfg(feature = "edonr")]
pub use edonr::{EdonR224, EdonR256, EdonR384, EdonR512};
#[cfg(feature = "blake")]
pub use keccak::{
    Keccak224, Keccak256, Keccak384, Keccak512, KeccakF200, KeccakF400, KeccakF800, KeccakF1600,
};
#[cfg(feature = "md2")]
pub use md2::Md2;
#[cfg(feature = "md4")]
pub use md4::Md4;
#[cfg(feature = "md5")]
pub use md5::Md5;
#[cfg(feature = "ripemd")]
pub use ripemd::{Ripemd128, Ripemd160, Ripemd256, Ripemd320};
#[cfg(feature = "sha0")]
pub use sha0::Sha0;
#[cfg(feature = "sha1")]
pub use sha1::Sha1;
#[cfg(feature = "sha2")]
pub use sha2::{Sha224, Sha256, Sha384, Sha512, Sha512Trunc224, Sha512Trunc256};
#[cfg(feature = "sha3")]
pub use sha3::{Sha3_224, Sha3_256, Sha3_384, Sha3_512, Shake128, Shake256};

pub use cryptographic_hash_function::CryptographicHashFunction;
